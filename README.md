[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://i.imgur.com/cwhDBkE.png)](https://twitter.com/kreezxil) [![](https://imgur.com/dWeRuXB.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

This is a port of the [UndergroundBiomesConstructs](https://minecraft.curseforge.com/projects/undergroundbiomesconstructs) mod to 1.12.2

Adds

*   24 kinds of stone
*   24 variants of gravel
*   24 kinds of sand
*   16 types of cobblestone
*   Bricks
*   Slabs
*   Walls
*   Stairs
*   Buttons
*   BOP's overgrown stones
*   Moss Cobblestones
*   Monster Eggs
*   Ores from Minecraft and Mods are (or can be) integrated to match the surrounding stone.

You may find some help on adding ores from mods [here](https://github.com/Aang23/UndergroundBiomesConstructs/wiki/Adding-Ores-from-other-Mods).

*Note on 1.14.x : Those builds are early alphas. Bugs are to be expected... Please report them on Github's issue tracker! If upgrading to build 32 from an older build, please delete your config/undergroundbiomes folder!*

**If you have a problem or feature request, please use Github's Issue Tracker, I'm more likely to react to that in a timely manner. If are wondering why so many versions were skipped, you may find them [here](https://minecraft.curseforge.com/projects/undergroundbiomesconstruct-fork). You may also contact me trough my [discord](https://discord.gg/qMWRccB).**
